Format: 1.0
Source: blur-network-gui
Binary: blur-network-gui
Architecture: all
Version: 0.1.8.3
Maintainer: The Blur Network <admin@blur.cash>
Homepage: https://blur.cash
Build-Depends: build-essential, debhelper (>= 9), git, cmake, libssl-dev, pkg-config, libboost-all-dev, libssl-dev, libhidapi-dev, libusb-1.0-0-dev, libzmq3-dev, libpgm-dev, libunbound-dev, libsodium-dev, qtbase5-dev, qt5-default, qtdeclarative5-dev, qml-module-qtquick-controls, qml-module-qtquick-controls2, qml-module-qtquick-dialogs, qml-module-qtquick-xmllistmodel, qml-module-qt-labs-settings, qml-module-qt-labs-folderlistmodel, qttools5-dev-tools
Package-List:
 blur-network-gui deb web optional arch=all
